﻿async function Fetch(_config) {
    let response;

    try {
        if (_config.type == 'GET')
            response = await fetch(_config.url, {
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                }
            });
        else if (_config.type == 'POST' || _config.type == 'PUT')
            response = await fetch(_config.url, {
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token'),
                    'Content-Type': 'application/json'
                },
                method: _config.type,
                body: _config.data
            });

        if (!response.ok) {
            if (response.status == 400) alert('¡Error!');
            else if (response.status == 401) alert('Token Expired!');
            else if (response.status == 403) alert('Forbidden!');

            throw new Error(response.status);
        }
        else if (response.status == 204) return;

        const data = await response.json();

        _config.success(data);
    }
    catch (error) {
        console.log(error)
    }
}