# Blog Engine

The Blog Engine project is made with asp.net framework, use bootstrap with framework to speed up the development process, also javascript and css.
## Configuration

To run the project you need:
- Clone with visual studio 2022 the project.
- If for some reason the API port changes, update the Web.config in the WebApi key, since the API url is specified here.

## What can you do?

In this project you can:
- Login with a user.
- View the published posts with their comments.
- Add comments.
- View the posts of a writer.
- A writer can create posts.
- A writer can edit posts.
- A writer can submit posts.
- A writer can edit a rejected post and submit again.
- An editor can see posts pending processing.
- An editor can approve posts.
- An editor can reject posts.

## Times

- 09/11/2022 -> 09:00 p.m. - 12:00 a.m.
- 10/11/2022 -> 10:00 a.m. - 02:00 a.m.
- 10/11/2022 -> 03:00 p.m. - 06:00 p.m.
